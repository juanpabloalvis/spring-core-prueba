package com.springbootrest.di.profiles;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile({"dev", "default"})
public class DevEnvironmentImpl implements EnvironmentService {
    @Override
    public String getEnvironment() {
        return "Dev";
    }
}
