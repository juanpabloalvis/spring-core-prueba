package com.springbootrest.di.profiles;

public interface EnvironmentService {

    String getEnvironment();

}
