package com.springbootrest.di.controllers;

import com.springbootrest.di.models.Patient;
import com.springbootrest.di.services.PatientService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/patients")
@CrossOrigin(origins={"http://localhost:3000", "http://127.0.0.1:8080", "http://localhost:8080"})
public class PatientController {

    @Autowired
    private PatientService patientService;

    private static final Logger log = LoggerFactory.getLogger(PatientController.class);


    @GetMapping //Handler methods: HTTP + Recurso
    @ApiOperation(value = "Return a patient list", response = Patient.class)
    public ResponseEntity<Page<Patient>> getPatients(@RequestParam(required = false, value = "page", defaultValue = "0") int page,
                                                  @RequestParam(required = false, value = "size", defaultValue = "100") int size) {
        return new ResponseEntity<Page<Patient>>(patientService.getPatients(page, size), HttpStatus.OK);
    }


    @GetMapping(value = "/{patientId}")
    @ApiOperation(value = "Return a patient for a given patient id", response = Patient.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message="The record was found"),
            @ApiResponse(code = 404, message="The record was not found")
    })

    public ResponseEntity<Patient> getPatientById(@PathVariable("patientId") Integer patientId) {
        return new ResponseEntity<Patient>(patientService.getPatientById(patientId), HttpStatus.OK);
    }

    @GetMapping(value = "/patientname/{patientname}")
    public ResponseEntity<Patient> getPatientByName(@PathVariable("patientname") String patientname) {
        return new ResponseEntity<Patient>(patientService.getPatientByName(patientname), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Patient> createPatient(@RequestBody Patient patient) {

        return new ResponseEntity<Patient>(patientService.createPatient(patient), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{patientId}")
    public ResponseEntity<Patient> updatePatient(@PathVariable("patientId") Integer patientId, @RequestBody Patient patient) {
        return new ResponseEntity<Patient>(patientService.updatePatient(patient, patientId), HttpStatus.OK);
    }

    @DeleteMapping("/{patientId}")
    public ResponseEntity<Void> deletePatient(@PathVariable("patientname") Integer patientId) {
        patientService.deletePatient(patientId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
