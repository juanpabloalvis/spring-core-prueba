package com.springbootrest.di;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCoreApplication {
    private static final Logger log = LoggerFactory.getLogger(SpringCoreApplication.class);

    public static void main(String[] args) {
        log.info("Starting app");
        SpringApplication.run(SpringCoreApplication.class, args);

    }

}
