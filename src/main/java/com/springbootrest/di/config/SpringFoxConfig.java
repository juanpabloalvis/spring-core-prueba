package com.springbootrest.di.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SpringFoxConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(getApiInfo())
                .select()
//                .apis(RequestHandlerSelectors.any()) // aquí muestra todos los controllers
                .apis(RequestHandlerSelectors.basePackage("com.springbootrest.di.controllers")) // aquí podemos filtrar que mostrar y qué no en swagger
                .paths(PathSelectors.any()) // aquí podría filtar solo cierta ruta
                .build();
    }

    /**
     * Este método permit configurar el título del swagger
     * @return un nuevo ApiInfoBuilder
     */
    private ApiInfo getApiInfo() {
        return new ApiInfoBuilder()
                .title("Servicios de Prueba con Spring")
                .version("1.0")
                .license("Apache 2.0")
                .contact(new Contact("Juan  Pablo","localhostporahora.com", "juan.alvis@probando.123"))
                .description("Api de prueba")
                .termsOfServiceUrl("Aplican condiciones y restricciones")
                .build();
    }

}