package com.springbootrest.di.config;


import com.github.javafaker.Faker;
import com.springbootrest.di.models.Patient;
import com.springbootrest.di.repositories.PatientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DbSettings implements ApplicationRunner {

    @Autowired
    private Faker faker;

    @Autowired
    private PatientRepository repository;


    private static final Logger log = LoggerFactory.getLogger(DbSettings.class);


    @Override
    public void run(ApplicationArguments args) throws Exception {

        log.info("create some patients");

        for (int i = 0; i < 10; i++) {
            Patient patient = new Patient();
            patient.setFirstName(faker.funnyName().name());
            patient.setLastName(faker.name().lastName());
            patient.setEmail(faker.internet().emailAddress());
            patient.setDocumentId(faker.idNumber().valid());
            patient.setPhone(String.valueOf(faker.number().randomNumber()));
            Patient save = repository.save(patient);

        }
    }


}
