package com.springbootrest.di.services;

import com.springbootrest.di.models.Patient;
import com.springbootrest.di.repositories.PatientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class PatientService {

    private static final Logger log = LoggerFactory.getLogger(PatientService.class);

    @Autowired
    private PatientRepository repository;

    public Page<Patient> getPatients(int page, int size) {
        Page<Patient> all = repository.findAll(PageRequest.of(page, size));
        return all;
    }


    public Patient getPatientById(Integer userId) {
        return repository.findById(userId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Patient with id %d does not exists", userId)));
    }

    public Patient getPatientByName(String firstName) {

        log.info("Busqueda de Pacientes por 'firstName'");
        return repository.findByFirstName(firstName).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Patient with firstName %s does not exists", firstName)));
    }


    public Patient createPatient(Patient patient) {
        if(patient.getId() == null){
            return repository.save(patient);
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Patient id should not provided", patient));

    }


    public Patient updatePatient(Patient role, Integer roleId) {
        Optional<Patient> result = repository.findById(roleId);
        if (result.isPresent()) {
            return repository.save(role);
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Patient id %d does not exists", roleId));
    }

    public void deletePatient(Integer userId) {
        Optional<Patient> result = repository.findById(userId);
        if (result.isPresent()) {
            repository.delete(result.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Patient id %d does not exists", userId));
        }
    }


}
