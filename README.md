# Prueba técnica Spring Docker 


### Link url swagger
http://localhost:8080/swagger-ui/#/

### Docker 
#### Obtener imagen de mysql y ejecutar la imagen
``` 
docker run --name mysql-standalone -e MYSQL_ROOT_PASSWORD=sa -e MYSQL_DATABASE=pacientes -e MYSQL_USER=sa -e MYSQL_PASSWORD=sa -d mysql:5.7
docker container logs mysql-standalone
docker exec -it mysql-standalone bash -l

``` 
#### Compilar y empaquetar el proyecto:
``` 
mvn clean package spring-boot:repackage
``` 
#### Crear el archivo "Dockerfile" con la inforamción de la imagen a construir:
``` 
FROM openjdk:11
ADD target/spring-core-prueba-0.0.1-SNAPSHOT.jar spring-core-prueba-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "spring-core-prueba-0.0.1-SNAPSHOT.jar"]
``` 
#### Construir la imagen:
``` 
docker build -t springboot-prueba .
``` 

#### Desplegar el micro servicio por el puerto 8080
``` 
docker run -d -p 8080:8080 --name springboot-prueba --link mysql-standalone:mysql springboot-prueba
docker container logs springboot-prueba
``` 
Ver definición de servicio por swagger, y ejecutar. http://localhost:8080/swagger-ui/#/

### Usuario de autenticacion por web
``` 
admin
admin
``` 

