FROM openjdk:11
ADD target/spring-core-prueba-0.0.1-SNAPSHOT.jar spring-core-prueba-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "spring-core-prueba-0.0.1-SNAPSHOT.jar"]